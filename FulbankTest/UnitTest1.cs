﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Fulbank;
using Fulbank.Model;

namespace FulbankTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDeposit()
        {
            float amount = 10;
            CurrentAccount testAccount = new CurrentAccount();
            testAccount.Balance = 5;
            testAccount.Deposit(amount);

            Assert.AreEqual(15,testAccount.Balance,"Deposit OK");
            //Assert.AreEqual(14, testAccount.Balance, "Deposit Fail");
        }
        [TestMethod]
        public void TestWithDrawal()
        {
            float amount = 10;
            CurrentAccount testAccount = new CurrentAccount();
            testAccount.Balance = 20;
            testAccount.Withdrawal(amount);

            Assert.AreEqual(10, testAccount.Balance, "WithDrawal OK");
        }
    }
}
