﻿namespace Fulbank.Forms
{
    partial class Ecran_ResetPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ecran_ResetPassword));
            this.Label_Mail = new System.Windows.Forms.Label();
            this.textBox_email = new System.Windows.Forms.TextBox();
            this.button_envoyer = new System.Windows.Forms.Button();
            this.Label_CodeSecret = new System.Windows.Forms.Label();
            this.textBox_codeSecret = new System.Windows.Forms.TextBox();
            this.Label_NewMDP = new System.Windows.Forms.Label();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.textBox_confirmpassword = new System.Windows.Forms.TextBox();
            this.Label_CheckMDP = new System.Windows.Forms.Label();
            this.button_resetPassword = new System.Windows.Forms.Button();
            this.groupBox_Reset = new System.Windows.Forms.GroupBox();
            this.Label_Titre = new System.Windows.Forms.Label();
            this.groupBox_Reset.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label_Mail
            // 
            this.Label_Mail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Label_Mail.AutoSize = true;
            this.Label_Mail.Location = new System.Drawing.Point(177, 38);
            this.Label_Mail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Mail.Name = "Label_Mail";
            this.Label_Mail.Size = new System.Drawing.Size(41, 13);
            this.Label_Mail.TabIndex = 0;
            this.Label_Mail.Text = "Email : ";
            // 
            // textBox_email
            // 
            this.textBox_email.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_email.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(67)))), ((int)(((byte)(153)))));
            this.textBox_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_email.Location = new System.Drawing.Point(240, 38);
            this.textBox_email.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_email.Name = "textBox_email";
            this.textBox_email.Size = new System.Drawing.Size(182, 20);
            this.textBox_email.TabIndex = 1;
            // 
            // button_envoyer
            // 
            this.button_envoyer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_envoyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(67)))), ((int)(((byte)(153)))));
            this.button_envoyer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_envoyer.ForeColor = System.Drawing.Color.White;
            this.button_envoyer.Location = new System.Drawing.Point(265, 68);
            this.button_envoyer.Margin = new System.Windows.Forms.Padding(2);
            this.button_envoyer.Name = "button_envoyer";
            this.button_envoyer.Size = new System.Drawing.Size(139, 43);
            this.button_envoyer.TabIndex = 2;
            this.button_envoyer.Text = "Envoyer";
            this.button_envoyer.UseVisualStyleBackColor = false;
            this.button_envoyer.Click += new System.EventHandler(this.button_envoyer_Click);
            // 
            // Label_CodeSecret
            // 
            this.Label_CodeSecret.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Label_CodeSecret.AutoSize = true;
            this.Label_CodeSecret.Location = new System.Drawing.Point(131, 200);
            this.Label_CodeSecret.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_CodeSecret.Name = "Label_CodeSecret";
            this.Label_CodeSecret.Size = new System.Drawing.Size(72, 13);
            this.Label_CodeSecret.TabIndex = 3;
            this.Label_CodeSecret.Text = "Code Secret :";
            // 
            // textBox_codeSecret
            // 
            this.textBox_codeSecret.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_codeSecret.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(67)))), ((int)(((byte)(153)))));
            this.textBox_codeSecret.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_codeSecret.Location = new System.Drawing.Point(240, 203);
            this.textBox_codeSecret.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_codeSecret.Name = "textBox_codeSecret";
            this.textBox_codeSecret.Size = new System.Drawing.Size(182, 20);
            this.textBox_codeSecret.TabIndex = 4;
            // 
            // Label_NewMDP
            // 
            this.Label_NewMDP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Label_NewMDP.AutoSize = true;
            this.Label_NewMDP.Location = new System.Drawing.Point(57, 235);
            this.Label_NewMDP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_NewMDP.Name = "Label_NewMDP";
            this.Label_NewMDP.Size = new System.Drawing.Size(126, 13);
            this.Label_NewMDP.TabIndex = 5;
            this.Label_NewMDP.Text = "Nouveau mot de passe : ";
            // 
            // textBox_password
            // 
            this.textBox_password.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(67)))), ((int)(((byte)(153)))));
            this.textBox_password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_password.Location = new System.Drawing.Point(240, 236);
            this.textBox_password.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.Size = new System.Drawing.Size(182, 20);
            this.textBox_password.TabIndex = 6;
            // 
            // textBox_confirmpassword
            // 
            this.textBox_confirmpassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_confirmpassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(67)))), ((int)(((byte)(153)))));
            this.textBox_confirmpassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_confirmpassword.Location = new System.Drawing.Point(240, 268);
            this.textBox_confirmpassword.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_confirmpassword.Name = "textBox_confirmpassword";
            this.textBox_confirmpassword.Size = new System.Drawing.Size(182, 20);
            this.textBox_confirmpassword.TabIndex = 7;
            // 
            // Label_CheckMDP
            // 
            this.Label_CheckMDP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Label_CheckMDP.AutoSize = true;
            this.Label_CheckMDP.Location = new System.Drawing.Point(52, 268);
            this.Label_CheckMDP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_CheckMDP.Name = "Label_CheckMDP";
            this.Label_CheckMDP.Size = new System.Drawing.Size(123, 13);
            this.Label_CheckMDP.TabIndex = 8;
            this.Label_CheckMDP.Text = "Confirmer mot de passe :";
            // 
            // button_resetPassword
            // 
            this.button_resetPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_resetPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(67)))), ((int)(((byte)(153)))));
            this.button_resetPassword.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_resetPassword.ForeColor = System.Drawing.Color.White;
            this.button_resetPassword.Location = new System.Drawing.Point(265, 303);
            this.button_resetPassword.Margin = new System.Windows.Forms.Padding(2);
            this.button_resetPassword.Name = "button_resetPassword";
            this.button_resetPassword.Size = new System.Drawing.Size(139, 43);
            this.button_resetPassword.TabIndex = 9;
            this.button_resetPassword.Text = "Réinitialiser";
            this.button_resetPassword.UseVisualStyleBackColor = false;
            this.button_resetPassword.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox_Reset
            // 
            this.groupBox_Reset.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox_Reset.AutoSize = true;
            this.groupBox_Reset.Controls.Add(this.textBox_codeSecret);
            this.groupBox_Reset.Controls.Add(this.button_resetPassword);
            this.groupBox_Reset.Controls.Add(this.Label_Mail);
            this.groupBox_Reset.Controls.Add(this.Label_CheckMDP);
            this.groupBox_Reset.Controls.Add(this.textBox_email);
            this.groupBox_Reset.Controls.Add(this.textBox_confirmpassword);
            this.groupBox_Reset.Controls.Add(this.button_envoyer);
            this.groupBox_Reset.Controls.Add(this.textBox_password);
            this.groupBox_Reset.Controls.Add(this.Label_CodeSecret);
            this.groupBox_Reset.Controls.Add(this.Label_NewMDP);
            this.groupBox_Reset.Location = new System.Drawing.Point(109, 51);
            this.groupBox_Reset.Name = "groupBox_Reset";
            this.groupBox_Reset.Size = new System.Drawing.Size(544, 364);
            this.groupBox_Reset.TabIndex = 10;
            this.groupBox_Reset.TabStop = false;
            // 
            // Label_Titre
            // 
            this.Label_Titre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Titre.AutoSize = true;
            this.Label_Titre.BackColor = System.Drawing.Color.Transparent;
            this.Label_Titre.ForeColor = System.Drawing.Color.White;
            this.Label_Titre.Location = new System.Drawing.Point(346, 24);
            this.Label_Titre.Name = "Label_Titre";
            this.Label_Titre.Size = new System.Drawing.Size(145, 13);
            this.Label_Titre.TabIndex = 10;
            this.Label_Titre.Text = "Modification de mot de passe";
            this.Label_Titre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Ecran_ResetPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(194)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(775, 486);
            this.Controls.Add(this.Label_Titre);
            this.Controls.Add(this.groupBox_Reset);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Ecran_ResetPassword";
            this.Text = "Reset Password";
            this.groupBox_Reset.ResumeLayout(false);
            this.groupBox_Reset.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_Mail;
        private System.Windows.Forms.TextBox textBox_email;
        private System.Windows.Forms.Button button_envoyer;
        private System.Windows.Forms.Label Label_CodeSecret;
        private System.Windows.Forms.TextBox textBox_codeSecret;
        private System.Windows.Forms.Label Label_NewMDP;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.TextBox textBox_confirmpassword;
        private System.Windows.Forms.Label Label_CheckMDP;
        private System.Windows.Forms.Button button_resetPassword;
        private System.Windows.Forms.GroupBox groupBox_Reset;
        private System.Windows.Forms.Label Label_Titre;
    }
}