﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fulbank.Forms
{
    
    public partial class Ecran_ResetPassword : Form
    {
        // Initialisation d'une nouvelle police d'écriture //
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
        IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font myFont;
        // FIN ICI //

        Person aPerson = new Person();
        public Ecran_ResetPassword()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized; //Plein écran
            SetPoliceEcriture();
            WaitChargementPage();
        }

        public string randomcode;
        public async Task WaitChargementPage()
        {
            await Task.Delay(50);
            //Console.WriteLine("testDealy");
            CentrerText();
        }

        public void SetPoliceEcriture()
        {
            // Importation de la police d'écriture Bungee //
            byte[] fontData = Properties.Resources.Bungee_Regular;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.Bungee_Regular.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.Bungee_Regular.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            myFont = new Font(fonts.Families[0], 15.0F);

            // Application de la police d'écriture //
            button_envoyer.Font = new Font(myFont.FontFamily, 10, FontStyle.Regular);
            button_resetPassword.Font = new Font(myFont.FontFamily, 10, FontStyle.Regular);

            Label_Mail.Font = new Font(FontFamily.GenericSansSerif, 12);
            Label_CodeSecret.Font = new Font(FontFamily.GenericSansSerif, 12);
            Label_NewMDP.Font = new Font(FontFamily.GenericSansSerif, 12);
            Label_CheckMDP.Font = new Font(FontFamily.GenericSansSerif, 12);

            Label_Titre.Font = new Font(myFont.FontFamily, 15, FontStyle.Regular);
        }

        public void CentrerText()
        {
            groupBox_Reset.Left = (this.Width / 2) - (groupBox_Reset.Width / 2);
            Label_Titre.Left = (this.Width / 2) - (Label_Titre.Width / 2);

            /*Label_Mail.Left = (this.Width / 2) - (Label_Mail.Width / 2);
            Label_CodeSecret.Left = (this.Width / 2) - (Label_CodeSecret.Width / 2);
            Label_NewMDP.Left = (this.Width / 2) - (Label_NewMDP.Width / 2);
            Label_CheckMDP.Left = (this.Width / 2) - (Label_CheckMDP.Width / 2);

            textBox_email.Left = (this.Width / 2) - (textBox_email.Width / 2);
            textBox_codeSecret.Left = (this.Width / 2) - (textBox_codeSecret.Width / 2);
            textBox_password.Left = (this.Width / 2) - (textBox_password.Width / 2);
            textBox_confirmpassword.Left = (this.Width / 2) - (textBox_confirmpassword.Width / 2);

            button_envoyer.Left = (this.Width / 2) - (button_envoyer.Width / 2);
            button_resetPassword.Left = (this.Width / 2) - (button_resetPassword.Width / 2);*/
        }

        private void button_envoyer_Click(object sender, EventArgs e)
        {
            // string from, pass, messagebody;
            Random random = new Random();
            randomcode = random.Next(999999).ToString();
            MailMessage message = new MailMessage();
            string to = (textBox_email.Text).ToString();
            var from = "fulbank28@gmail.com";
            var pass = "8f$CLpE5XzzAPD";
            var messagebody = $"Votre code secret : {randomcode}";
            message.To.Add(to);
            message.From = new MailAddress(from);
            message.Body = messagebody;
            message.Subject = "Reinitialisation de mot de passe";
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential(from, pass);
            try
            {
                smtp.Send(message);
                MessageBox.Show("Code secret envoyé");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            if(randomcode == textBox_codeSecret.Text && textBox_password.Text == textBox_confirmpassword.Text)
            {
                try
                {
                    aPerson.Mail = textBox_email.Text;
                    aPerson.Password = textBox_password.Text;
                    aPerson.ResetPassword();
                    MessageBox.Show("Votre mot de passe à été réinitialisé");
                    Ecran_Connexion ecran_Connexion = new Ecran_Connexion();
                    this.Hide();
                    ecran_Connexion.ShowDialog();
                    this.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Code secret ou mot de passe incorrect");
            }
        }
    }
}
