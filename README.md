# Fulbank

Le projet Fulbank a été créé dans le but d’aider une banque fictive à développer un logiciel de gestion de compte bancaire des usagers accessible sur des bornes au sein de la banque. Sur ces bornes il est également possible de gérer de la cryptomonnaie. Une authentification est nécessaire pour accéder à ses comptes.

**Author** : Paul ARNAUD and Axel BLANCHARD

## Configuration de l'application

## Création BDD 
Pour utiliser l'application il faut ajouter la base de données dans un SGBD

La base de donnée se trouve dans le fichier fulbank.sql

## Connexion BDD

Création fichier bdd.ini :
```
[Fulbank]
host=nom_host
database=nom_bdd
username=username
password=password
```

Le fichier bdd.ini doit se retrouver à la racine du projet

Vous pouvez modifier son emplacement dans le code dans la page Ecran_BDD.cs à la ligne 71